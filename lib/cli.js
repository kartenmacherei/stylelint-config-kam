#!/usr/bin/env node

// Import 3rd party tools
const chalk = require('chalk');

// Import own tools
const commandLineTool = require('./tools/command-line');

// Colors for the logs
const green = chalk.bold.green;

console.log(green('Start linting'));

// Get the settings from the cli
const usingAutofix = commandLineTool.isAutoFixOptionOn();
const paths = commandLineTool.getPaths(usingAutofix);

// Initialize Stylelint with KAM rules
const kamRules = require('../index.js');
const stylelint = require('stylelint');

stylelint.lint({
	config: kamRules,
	files: paths,
	formatter: 'string',
	fix: usingAutofix
})
	.then(function(data) {
		// do things with data.output, data.errored and data.results
		console.log(data.output);
		console.log(green('Linting done'));

		if (data.errored) {
			process.exit(1);
		}

		process.exit(0);

	})
	.catch(function(err) {
		// do things with err e.g.
		console.error(err.stack);
		process.exit(1);
	});
