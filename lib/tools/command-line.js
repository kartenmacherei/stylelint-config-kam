#!/usr/bin/env node
const CLI_COMMAND_POSITION_FIX = 2;
const CLI_COMMAND_POSITION_PATHS_AUTOFIX = 3;
const CLI_COMMAND_POSITION_PATHS = 2;

module.exports = {

	isAutoFixOptionOn() {
		return process.argv[CLI_COMMAND_POSITION_FIX] === '-f' || process.argv[CLI_COMMAND_POSITION_FIX] === '--fix';
	},

	getPaths(usingAutofix) {

		let paths = [];

		if (usingAutofix) {
			paths = process.argv.slice(CLI_COMMAND_POSITION_PATHS_AUTOFIX);
		} else {
			paths = process.argv.slice(CLI_COMMAND_POSITION_PATHS);
		}

		return paths;
	}

};
