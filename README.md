# KAM CSS standards (Stylelint)

Package to unify Stylelint version and rules across projects. No need to
install stylelint anymore in your projects and maintain them all separatly.
This package will provide you with stylelint itself and with the rules we
follow at KAM

## Install
```
npm install -D stylelint-config-kam
```

or
```
yarn add -D stylelint-config-kam
```

## Usage
Create the file `stylelint.config.js` in your root folder and extend kam rules
```
module.exports = {
    ...
	extends: 'stylelint-config-kam'
};
```

- For just linting and showing the report, run:
```
kamstylelint path/to/folder
```

- For linting and autofix the problems (works either with -f or --fix), run:
```
kamstylelint --fix path/to/folder [another/path/to/folder]
```

**Note**: *The path can be a glob*

#### Recommended
- Add to your `package.json` *scripts* section the two following scripts:
```
"stylelint": "kamstylelint path/to/folder",
"stylelint:fix": "kamstylelint -f path/to/folder",
```

- Configure your IDE to use this rules so you don't need to be running
 the scripts while developing

## KAM rules
[Rules](index.js)

## Update the package
To update this package when a new version is ready, run:
```
npm update stylelint-config-kam
```

or

```
yarn upgrade stylelint-config-kam
```

## Changelog
[Changelog](CHANGELOG.md)

## Contributing

[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/dwyl/esta/issues)

**Please remember to increment the version on `package.json` following the SemVer pattern.**

1. Create a new branch from master following the next pattern:
```
feature/[TICKET-NUM-]short-description
```
Example: `feature/KAM-123-add-rule-foobar`

**Note:** *If there is no ticket then just do `feature/add-rule-foobar`*

2. Create a PR agains master. You will need to have at least one approval
and the inspection job from the pipeline needs to pass correctly.

3. When merging PR to master, a Bitbucket pipeline will inspect the code and if everything
is correct it will generate a new tag and push it. This tag number corresponds
to the verison npm uses to install/update the package (version in `package.json`).
If the tag is created correctly then it will be automatically publish on npm registry

## License
This software is licensed under the terms of the [MIT license](https://opensource.org/licenses/MIT). See [LICENSE.md](LICENSE.md) for the full license.
