# Changelog

All notable changes are documented in this file using the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## [1.0.0] - 2018-07-13

### Added

* First version of KAM Stylelint rules
